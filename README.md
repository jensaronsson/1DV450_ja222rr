1DV450_ja222rr
==============
## ***** *** ** New REPO with submodules.   ** *** *****

## Instructions for cloning.

To recieve all submodule you must append --recursive to git clone command.

  * git clone --recursive https://github.com/enpere/1DV450_ja222rr.git
 
## Instuctions for Zip

 * To download zip, you need to go inside each repo and download them separately :/ 
 
 **Links to each repo zip download link**
 
 * Link to zip - angular  -> https://github.com/enpere/angular/archive/master.zip
 * Link to zip - api      -> https://github.com/enpere/api/archive/master.zip
   

### Repo with submodules for each projects required in course 1dv450 

**Backend:**
* RailsApi
* Ruby on Rails

**Frontend:**
* ClientToerh
* AngularJs
